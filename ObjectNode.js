class ObjectNode {
    constructor(geometry, normals, parentJoint) {
      this.geometry = geometry;
      this.geometryManipulated = []; // to store manipulated geo
      this.normals = normals;
      this.normalsManipulated = []; // to store manipulated geo
      
      // parent joint point or a rotation point.
      // For root node it will be the center of the object
      this.parentJoint = parentJoint;
      this.parentJointManipulated = []; // to store manipulated parent joint
      this.flipCoords();
      
      this.sibling = null;
      this.child = null;
      
      deepCopy(this.geometry, this.geometryManipulated);
      deepCopy(this.normals, this.normalsManipulated);
      deepCopy(this.parentJoint, this.parentJointManipulated);
    }

    flipCoords() {
      var matrix = m4.xRotation(Math.PI);
      var vector = m4.transformPoint(matrix, [
        this.parentJoint[0],
        this.parentJoint[1],
        this.parentJoint[2],
      ])
      this.parentJoint[0] = vector[0];
      this.parentJoint[1] = vector[1];
      this.parentJoint[2] = vector[2];
      for (var ii = 0; ii < this.geometry.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.geometry[ii + 0],
          this.geometry[ii + 1],
          this.geometry[ii + 2],
          1,
        ]);
        this.geometry[ii + 0] = vector[0];
        this.geometry[ii + 1] = vector[1];
        this.geometry[ii + 2] = vector[2];
      }
      for (var ii = 0; ii < this.normals.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.normals[ii + 0],
          this.normals[ii + 1],
          this.normals[ii + 2],
          1,
        ]);
        this.normals[ii + 0] = vector[0];
        this.normals[ii + 1] = vector[1];
        this.normals[ii + 2] = vector[2];
      }
    }
  
    setSibling(sibling) {
      this.sibling = sibling;
    }
  
    setChild(child) {
      this.child = child;
    }
  
    translate(x, y, z) {
      for(var i = 0; i < this.geometryManipulated.length; i+=3) {
        this.geometryManipulated[i] = this.geometryManipulated[i] + x;
        this.geometryManipulated[i+1] = this.geometryManipulated[i+1] + y;
        this.geometryManipulated[i+2] = this.geometryManipulated[i+2] + z;
      }
      this.parentJointManipulated[0] = this.parentJointManipulated[0] + x;
      this.parentJointManipulated[1] = this.parentJointManipulated[1] + y;
      this.parentJointManipulated[2] = this.parentJointManipulated[2] + z;
      if(this.child != null) this.child.translateChild(x, y, z);
    }
  
    // this is only called by objects, not by public
    translateChild(x, y, z) {
      this.translate(x,y,z);
      if(this.sibling != null) this.sibling.translateChild(x,y,z);
    }
  
    rotate(x, y, z) {
      var matrix = m4.identity();
      matrix = m4.xRotate(matrix, x);
      matrix = m4.yRotate(matrix, y);
      matrix = m4.zRotate(matrix, z);
      for (var ii = 0; ii < this.geometryManipulated.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.geometryManipulated[ii + 0] - this.parentJointManipulated[0],
          this.geometryManipulated[ii + 1] - this.parentJointManipulated[1],
          this.geometryManipulated[ii + 2] - this.parentJointManipulated[2],
          1,
        ]);
        this.geometryManipulated[ii + 0] = this.parentJointManipulated[0] + vector[0];
        this.geometryManipulated[ii + 1] = this.parentJointManipulated[1] + vector[1];
        this.geometryManipulated[ii + 2] = this.parentJointManipulated[2] + vector[2];
      }
  
      // rotate normal
      for (var ii = 0; ii < this.normalsManipulated.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.normalsManipulated[ii + 0],
          this.normalsManipulated[ii + 1],
          this.normalsManipulated[ii + 2],
          1,
        ]);
        this.normalsManipulated[ii + 0] = vector[0];
        this.normalsManipulated[ii + 1] = vector[1];
        this.normalsManipulated[ii + 2] = vector[2];
      }
      if(this.child != null) this.child.rotateChild(matrix, this.parentJointManipulated);
    }
  
    // this is only called by objects, not by public
    rotateChild(matrix, rotationPoint) {
      for (var ii = 0; ii < this.geometryManipulated.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.geometryManipulated[ii + 0] - rotationPoint[0],
          this.geometryManipulated[ii + 1] - rotationPoint[1],
          this.geometryManipulated[ii + 2] - rotationPoint[2],
          1,
        ]);
        this.geometryManipulated[ii + 0] = rotationPoint[0] + vector[0];
        this.geometryManipulated[ii + 1] = rotationPoint[1] + vector[1];
        this.geometryManipulated[ii + 2] = rotationPoint[2] + vector[2];
      }
  
      // rotate normal
      for (var ii = 0; ii < this.normalsManipulated.length; ii += 3) {
        var vector = m4.transformPoint(matrix, [
          this.normalsManipulated[ii + 0],
          this.normalsManipulated[ii + 1],
          this.normalsManipulated[ii + 2],
          1,
        ]);
        this.normalsManipulated[ii + 0] = vector[0];
        this.normalsManipulated[ii + 1] = vector[1];
        this.normalsManipulated[ii + 2] = vector[2];
      }
  
      // rotate parent joint
      var vector = m4.transformPoint(matrix, [
        this.parentJointManipulated[0] - rotationPoint[0],
        this.parentJointManipulated[1] - rotationPoint[1],
        this.parentJointManipulated[2] - rotationPoint[2],
      ])
      this.parentJointManipulated[0] = rotationPoint[0] + vector[0];
      this.parentJointManipulated[1] = rotationPoint[1] + vector[1];
      this.parentJointManipulated[2] = rotationPoint[2] + vector[2];
      if(this.sibling != null) this.sibling.rotateChild(matrix, rotationPoint);
      if(this.child != null) this.child.rotateChild(matrix, rotationPoint);
    }
  
    getGeometry() {
      var temp = this.geometryManipulated;
      if(this.sibling != null) temp = temp.concat(this.sibling.getGeometry());
      if(this.child != null) temp = temp.concat(this.child.getGeometry());
      return temp;
    }
  
    getNormals() {
      var temp = this.normalsManipulated;
      if(this.sibling != null) temp = temp.concat(this.sibling.getNormals());
      if(this.child != null) temp = temp.concat(this.child.getNormals());
      return temp;
    }
    reset() {
      deepCopy(this.geometry, this.geometryManipulated);
      deepCopy(this.normals, this.normalsManipulated);
      deepCopy(this.parentJoint, this.parentJointManipulated);
    }
  }
function deepCopy(source, target) {
  for(var i = 0; i < source.length; i++) {
    target[i] = source[i] + 1 - 1; // to force deep copy instead of shallow
  }
}