/**
 * Object: Dragon
 * Desc: Dragon/Snake-like
 * Joints:
 * 1. head-neck
 * 2. head-lowerJaw
 * 3. lowerJaw-neck
 * 4. neck-body1
 * 5. body1-body2
 * 6. body2-body3
 * 7. body3-body4
 * 8. body4-body5
 * 
 * Object Tree:
 *          head
 *          /
 *        neck ----- lowerJaw
 *       /
 *      body1
 *      /
 *     body2
 *     /
 *    body3
 *     /
 *   body4
 *    /
 *  body5
 * 
 */

"use strict";

var canvas;
var gl;

var primitiveType;
var offset = 0;
var count = 132;

var angleCam = 0;
var angleFOV = 60;
var fRotationRadians = 0;

var matrix;

var translationMatrix;
var rotationMatrix;
var scaleMatrix;
var projectionMatrix;
var cameraMatrix;
var viewMatrix;
var viewProjectionMatrix;

var worldViewProjectionMatrix;
var worldInverseTransposeMatrix;
var worldInverseMatrix;
var worldMatrix;

var FOV_Radians; //field of view
var aspect; //projection aspect ratio
var zNear; //near view volume
var zFar; //far view volume

var cameraPosition = [100, 0, 200]; //eye/camera coordinates
var UpVector = [0, 1, 0]; //up vector
var fPosition = [0, 0, 0]; //at

var worldViewProjectionLocation;
var worldInverseTransposeLocation;
var colorLocation;
var lightWorldPositionLocation;
var worldLocation;

// list of all object to make dragon
var object; // root of the object / head
var lowerJaw;
var neck;
var body1;
var body2;
var body3;
var body4;
var body5;

// set the default rotation for each object
var objectXRot = 0, objectYRot = 0, objectZRot = 0,
lowerJawXRot = 0, lowerJawYRot = 0, lowerJawZRot = 0,
neckXRot = 0, neckYRot = 0, neckZRot = 0,
body1XRot = 0, body1YRot = 0, body1ZRot = 0,
body2XRot = 0, body2YRot = 0, body2ZRot = 0,
body3XRot = 0, body3YRot = 0, body3ZRot = 0,
body4XRot = 0, body4YRot = 0, body4ZRot = 0,
body5XRot = 0, body5YRot = 0, body5ZRot = 0;

// animations variables
var animationToggle = false;
var angle = 0; // to store angle for animation

// what user currently select
var objectSelected;
var xRotationValue;
var yRotationValue;
var zRotationValue;

var program;

window.onload = function init() {
  canvas = document.getElementById("gl-canvas");

  gl = canvas.getContext("webgl2");
  if (!gl) alert("WebGL 2.0 isn't available");

  //  Configure WebGL

  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(1.0, 1.0, 1.0, 1.0);

  gl.enable(gl.CULL_FACE); //enable depth buffer
  gl.enable(gl.DEPTH_TEST);

  //initial default

  fRotationRadians = degToRad(0);
  FOV_Radians = degToRad(60);
  aspect = canvas.width / canvas.height;
  zNear = 1;
  zFar = 2000;

  initDragon();

  projectionMatrix = m4.perspective(FOV_Radians, aspect, zNear, zFar); //setup perspective viewing volume

  //  Load shaders and initialize attribute buffers
  program = initShaders(gl, "vertex-shader", "fragment-shader");
  gl.useProgram(program);

  // Load the data into the GPU

  worldViewProjectionLocation = gl.getUniformLocation(
    program,
    "u_worldViewProjection"
  );
  worldInverseTransposeLocation = gl.getUniformLocation(
    program,
    "u_worldInverseTranspose"
  );
  colorLocation = gl.getUniformLocation(program, "u_color");
  lightWorldPositionLocation = gl.getUniformLocation(
    program,
    "u_lightWorldPosition"
  );
  worldLocation = gl.getUniformLocation(program, "u_world");

  //update FOV
  fRotationRadians = degToRad(angleCam);

  xRotationValue = document.getElementById("xRotation");
  yRotationValue = document.getElementById("yRotation");
  zRotationValue = document.getElementById("zRotation");
  document.getElementById("object").value = objectSelected;
  document.getElementById("animate").onclick = function (event) {
    animationToggle = !animationToggle;
    render();
  }
  document.getElementById("object").onchange = function (event) {
    objectSelected = event.target.value;
    switch (objectSelected) {
      case "0":
        xRotationValue.value = radToDeg(objectXRot);
        yRotationValue.value = radToDeg(objectYRot);
        zRotationValue.value = radToDeg(objectZRot);
        break;
      case "1":
        xRotationValue.value = radToDeg(lowerJawXRot);
        yRotationValue.value = radToDeg(lowerJawYRot);
        zRotationValue.value = radToDeg(lowerJawZRot);
        break;
      case "2":
        xRotationValue.value = radToDeg(neckXRot);
        yRotationValue.value = radToDeg(neckYRot);
        zRotationValue.value = radToDeg(neckZRot);
        break;
      case "3":
        xRotationValue.value = radToDeg(body1XRot);
        yRotationValue.value = radToDeg(body1YRot);
        zRotationValue.value = radToDeg(body1ZRot);
        break;
      case "4":
        xRotationValue.value = radToDeg(body2XRot);
        yRotationValue.value = radToDeg(body2YRot);
        zRotationValue.value = radToDeg(body2ZRot);
        break;
      case "5":
        xRotationValue.value = radToDeg(body3XRot);
        yRotationValue.value = radToDeg(body3YRot);
        zRotationValue.value = radToDeg(body3ZRot);
        break;
      case "6":
        xRotationValue.value = radToDeg(body4XRot);
        yRotationValue.value = radToDeg(body4YRot);
        zRotationValue.value = radToDeg(body4ZRot);
        break;
      case "7":
        xRotationValue.value = radToDeg(body5XRot);
        yRotationValue.value = radToDeg(body5YRot);
        zRotationValue.value = radToDeg(body5ZRot);
        break;
    }
  }
  document.getElementById("xRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        objectXRot = degToRad(event.target.value);
        break;
      case "1":
        lowerJawXRot = degToRad(event.target.value);
        break;
      case "2":
        neckXRot = degToRad(event.target.value);
        break;
      case "3":
        body1XRot = degToRad(event.target.value);
        break;
      case "4":
        body2XRot = degToRad(event.target.value);
        break;
      case "5":
        body3XRot = degToRad(event.target.value);
        break;
      case "6":
        body4XRot = degToRad(event.target.value);
        break;
      case "7":
        body5XRot = degToRad(event.target.value);
        break;
    }
    render();
  }
  document.getElementById("yRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        objectYRot = degToRad(event.target.value);
        break;
      case "1":
        lowerJawYRot = degToRad(event.target.value);
        break;
      case "2":
        neckYRot = degToRad(event.target.value);
        break;
      case "3":
        body1YRot = degToRad(event.target.value);
        break;
      case "4":
        body2YRot = degToRad(event.target.value);
        break;
      case "5":
        body3YRot = degToRad(event.target.value);
        break;
      case "6":
        body4YRot = degToRad(event.target.value);
        break;
      case "7":
        body5YRot = degToRad(event.target.value);
        break;
    }
    render();
  };
  document.getElementById("zRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        objectZRot = degToRad(event.target.value);
        break;
      case "1":
        lowerJawZRot = degToRad(event.target.value);
        break;
      case "2":
        neckZRot = degToRad(event.target.value);
        break;
      case "3":
        body1ZRot = degToRad(event.target.value);
        break;
      case "4":
        body2ZRot = degToRad(event.target.value);
        break;
      case "5":
        body3ZRot = degToRad(event.target.value);
        break;
      case "6":
        body4ZRot = degToRad(event.target.value);
        break;
      case "7":
        body5ZRot = degToRad(event.target.value);
        break;
    }
    render();
  };
  primitiveType = gl.TRIANGLES;
  render(); //default render
};

function render() {
  // Compute the camera's matrix using look at.
  cameraMatrix = m4.lookAt(cameraPosition, fPosition, UpVector);

  // Make a view matrix from the camera matrix
  viewMatrix = m4.inverse(cameraMatrix);

  // Compute a view projection matrix
  viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

  worldMatrix = m4.identity();

  // Multiply the matrices.
  worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, worldMatrix);
  worldInverseMatrix = m4.inverse(worldMatrix);
  worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);

  // Set the matrices
  gl.uniformMatrix4fv(
    worldViewProjectionLocation,
    false,
    worldViewProjectionMatrix
  );
  gl.uniformMatrix4fv(
    worldInverseTransposeLocation,
    false,
    worldInverseTransposeMatrix
  );
  gl.uniformMatrix4fv(worldLocation, false, worldMatrix);

  // Set the color to use
  gl.uniform4fv(colorLocation, [0.2, 1, 0.2, 1]); // green

  // set the light direction.
  gl.uniform3fv(lightWorldPositionLocation, [100, 0, 100]);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // since we will compute each object independently,
  // we manipulate vertices before sending to GPU
  verticesManipulation();
  angle += 1;
  
  // init light vertices and normals
  var lightVertices = initLight(100, 0, 100, 5);
  var lightNormals = initLightNormals(lightVertices);

  // send manipulated vertices to GPU
  var n = initBuffers(new Float32Array(object.getGeometry().concat(lightVertices)));
  initNormals(new Float32Array(object.getNormals().concat(lightNormals)));
  //var n = initBuffers(new Float32Array(object.getGeometry()));
  //initNormals(new Float32Array(object.getNormals()));

  // draw
  gl.drawArrays(primitiveType, offset, n);

  // reset object to be transform for later on
  resetObject();

  // re-render for animation
  if(animationToggle) requestAnimationFrame(render);
}

function verticesManipulation() {
  if(animationToggle) {
    var angleInRadians = 360 - (angle * Math.PI) / 180;
    var sin = Math.sin(angleInRadians);
    body5.rotate(-sin * Math.PI/2, 0, 0);
    body4.rotate(sin * Math.PI/2, 0, 0);
    body3.rotate(-sin * Math.PI/2, 0, 0);
    body2.rotate(sin * Math.PI/2, 0, 0);
    body1.rotate(-sin * Math.PI/4, 0, 0);
    var y = 20 * sin;
    lowerJaw.rotate(Math.PI/10 + (sin * Math.PI/10), 0, 0);
    object.translate(0, y, 0);
  } else {
    body5.rotate(body5XRot, body5YRot,body5ZRot);
    body4.rotate(body4XRot, body4YRot,body4ZRot);
    body3.rotate(body3XRot, body3YRot,body3ZRot);
    body2.rotate(body2XRot, body2YRot,body2ZRot);
    body1.rotate(body1XRot, body1YRot,body1ZRot);
    lowerJaw.rotate(lowerJawXRot, lowerJawYRot, lowerJawZRot);
    neck.rotate(neckXRot, neckYRot, neckZRot);
    object.rotate(objectXRot, objectYRot, objectZRot);
  }
}

function resetObject() {
  // reset all object to default position for later
  // transformations
  object.reset();
  neck.reset();
  lowerJaw.reset();
  body1.reset();
  body2.reset();
  body3.reset();
  body4.reset();
  body5.reset();
}

/* Create buffers for multiple objects */
function initBuffers(vertices) {
  var n = vertices.length / 3;

  var positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var positionLocation = gl.getAttribLocation(program, "a_position");
  gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(positionLocation);

  return n;
}

/* Create normal buffers for multiple objects */
function initNormals(normals) {
  var normalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);

  // Associate out shader variables with our data buffer

  var normalLocation = gl.getAttribLocation(program, "a_normal");
  gl.vertexAttribPointer(normalLocation, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(normalLocation);
}

function radToDeg(r) {
  return (r * 180) / Math.PI;
}

function degToRad(d) {
  return (d * Math.PI) / 180;
}

function initDragon() {
  // head
  object = new ObjectNode([
    // left side of face
    0, 0, 0,
    0, 40, -60,
    15, 40, 0,
    // right side of face
    0, 0, 0,
    -15, 40, 0,
    0, 40, -60,
    // inner of upper jaw
    0, 40, -60,
    -15, 40, 0,
    15, 40, 0,
    // backside
    0, 0, 0,
    15, 40, 0,
    -15, 40, 0,
  ],[
    // left side of face
    0.91168, -0.34188, -0.22792,
    0.91168, -0.34188, -0.22792,
    0.91168, -0.34188, -0.22792,
    // right side of face
    -0.91168, -0.34188, -0.22792,
    -0.91168, -0.34188, -0.22792,
    -0.91168, -0.34188, -0.22792,
    // inner jaw 
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    // backside
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ],[0,40,0]);

  // lower jaw
  lowerJaw = new ObjectNode([
    // left side of lower jaw
    0, 60, 0,
    10, 40, 0,
    0, 40, -50,
    // right side of lower jaw
    0, 60, 0,
    0, 40, -50,
    -10, 40, 0,
    // inner of lower jaw
    10, 40, 0,
    -10, 40, 0,
    0, 40, -50,
    // backside
    0, 60, 0,
    -10, 40, 0,
    10, 40, 0,
  ],[
    // left side of lower jaw
    0.88045, 0.44023, -0.17609,
    0.88045, 0.44023, -0.17609,
    0.88045, 0.44023, -0.17609,
    // right side of lower jaw
    -0.88045, 0.44023, -0.17609,
    -0.88045, 0.44023, -0.17609,
    -0.88045, 0.44023, -0.17609,
    // inner of lower jaw
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    // backside
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ],[0,40,0]);
  
  // neck
  neck = new ObjectNode([
    // top
    7.5, 20, 0,
    7.5, 20, 30,
    -7.5, 20, 0,
    7.5, 20, 30,
    -7.5, 20, 30,
    -7.5, 20, 0,
    // left
    7.5, 20, 0,
    7.5, 50, 0,
    7.5, 20, 30,
    7.5, 50, 0,
    7.5, 50, 30,
    7.5, 20, 30,
    // bottom
    7.5, 50, 0,
    -7.5, 50, 0,
    7.5, 50, 30,
    -7.5, 50, 0,
    -7.5, 50, 30,
    7.5, 50, 30,
    // right
    -7.5, 20, 0,
    -7.5, 20, 30,
    -7.5, 50, 0,
    -7.5, 20, 30,
    -7.5, 50, 30,
    -7.5, 50, 0,
    // front
    7.5, 20, 0,
    -7.5, 20, 0,
    7.5, 50, 0,
    -7.5, 20, 0,
    -7.5, 50, 0,
    7.5, 50, 0,
    // back
    7.5, 20, 30,
    7.5, 50, 30,
    -7.5, 20, 30,
    7.5, 50, 30,
    -7.5, 50, 30,
    -7.5, 20, 30,
  ],[
    // top
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    // left
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    // bottom
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    // right
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    // front
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    // back
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ],[0,40,0]);

  var bodyGeo = [
    // top
    7.5, 20, 30,
    7.5, 20, 80,
    -7.5, 20, 30,
    7.5, 20, 80,
    -7.5, 20, 80,
    -7.5, 20, 30,
    // left
    7.5, 20, 30,
    7.5, 50, 30,
    7.5, 20, 80,
    7.5, 50, 30,
    7.5, 50, 80,
    7.5, 20, 80,
    // bottom
    7.5, 50, 30,
    -7.5, 50, 30,
    7.5, 50, 80,
    -7.5, 50, 30,
    -7.5, 50, 80,
    7.5, 50, 80,
    // right
    -7.5, 20, 30,
    -7.5, 20, 80,
    -7.5, 50, 30,
    -7.5, 20, 80,
    -7.5, 50, 80,
    -7.5, 50, 30,
    // front
    7.5, 20, 30,
    -7.5, 20, 30,
    7.5, 50, 30,
    -7.5, 20, 30,
    -7.5, 50, 30,
    7.5, 50, 30,
    // back
    7.5, 20, 80,
    7.5, 50, 80,
    -7.5, 20, 80,
    7.5, 50, 80,
    -7.5, 50, 80,
    -7.5, 20, 80,
  ];
  var bodyNormal = [
    // top
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    // left
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    // bottom
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    // right
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    // front
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    // back
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ];
  body1 = new ObjectNode([
    // top
    7.5, 20, 30,
    7.5, 20, 80,
    -7.5, 20, 30,
    7.5, 20, 80,
    -7.5, 20, 80,
    -7.5, 20, 30,
    // left
    7.5, 20, 30,
    7.5, 50, 30,
    7.5, 20, 80,
    7.5, 50, 30,
    7.5, 50, 80,
    7.5, 20, 80,
    // bottom
    7.5, 50, 30,
    -7.5, 50, 30,
    7.5, 50, 80,
    -7.5, 50, 30,
    -7.5, 50, 80,
    7.5, 50, 80,
    // right
    -7.5, 20, 30,
    -7.5, 20, 80,
    -7.5, 50, 30,
    -7.5, 20, 80,
    -7.5, 50, 80,
    -7.5, 50, 30,
    // front
    7.5, 20, 30,
    -7.5, 20, 30,
    7.5, 50, 30,
    -7.5, 20, 30,
    -7.5, 50, 30,
    7.5, 50, 30,
    // back
    7.5, 20, 80,
    7.5, 50, 80,
    -7.5, 20, 80,
    7.5, 50, 80,
    -7.5, 50, 80,
    -7.5, 20, 80,
  ],[
    // top
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    // left
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    // bottom
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    // right
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    // front
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    // back
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ],[0,35,30]);

  var body2Geo = [];
  var body2Normal = [];
  for(var iii = 0; iii < bodyGeo.length; iii++) {
    body2Geo[iii] = bodyGeo[iii] + 1 - 1; // to force not shallow copy
  }
  for(var iii = 2; iii < body2Geo.length; iii+=3) {
    body2Geo[iii] = body2Geo[iii] + 50;
  }
  for(var iii = 0; iii < bodyNormal.length; iii++) {
    body2Normal[iii] = bodyNormal[iii] + 1 - 1; // to force not shallow copy
  }
  body2 = new ObjectNode(body2Geo, body2Normal, [0,35,80]);

  var body3Geo = [];
  var body3Normal = [];
  for(var iii = 0; iii < bodyGeo.length; iii++) {
    body3Geo[iii] = bodyGeo[iii] + 1 - 1; // to force not shallow copy
  }
  for(var iii = 2; iii < body3Geo.length; iii+=3) {
    body3Geo[iii] = body3Geo[iii] + 100;
  }
  for(var iii = 0; iii < bodyNormal.length; iii++) {
    body3Normal[iii] = bodyNormal[iii] + 1 - 1; // to force not shallow copy
  }
  body3 = new ObjectNode(body3Geo, body3Normal, [0,35,130]);

  var body4Geo = [];
  var body4Normal = [];
  for(var iii = 0; iii < bodyGeo.length; iii++) {
    body4Geo[iii] = bodyGeo[iii] + 1 - 1; // to force not shallow copy
  }
  for(var iii = 2; iii < body4Geo.length; iii+=3) {
    body4Geo[iii] = body4Geo[iii] + 150;
  }
  for(var iii = 0; iii < bodyNormal.length; iii++) {
    body4Normal[iii] = bodyNormal[iii] + 1 - 1; // to force not shallow copy
  }
  body4 = new ObjectNode(body4Geo, body4Normal, [0,35,180]);

  var body5Geo = [];
  var body5Normal = [];
  for(var iii = 0; iii < bodyGeo.length; iii++) {
    body5Geo[iii] = bodyGeo[iii] + 1 - 1; // to force not shallow copy
  }
  for(var iii = 2; iii < body5Geo.length; iii+=3) {
    body5Geo[iii] = body5Geo[iii] + 200;
  }
  for(var iii = 0; iii < bodyNormal.length; iii++) {
    body5Normal[iii] = bodyNormal[iii] + 1 - 1; // to force not shallow copy
  }
  body5 = new ObjectNode(body5Geo, body5Normal, [0,35,230]);

  object.setChild(neck);
  neck.setSibling(lowerJaw);
  neck.setChild(body1);
  body1.setChild(body2);
  body2.setChild(body3);
  body3.setChild(body4);
  body4.setChild(body5);
}

function initLight(x, y, z, scale) {
  var SPHERE_DIV = 12;
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;

  // Vertices
  var vertices = [];
  for (j = 0; j <= SPHERE_DIV; j++) {
    aj = j * Math.PI / SPHERE_DIV;
    sj = Math.sin(aj);
    cj = Math.cos(aj);
    for (i = 0; i < SPHERE_DIV; i++) {
      ai = i * 2 * Math.PI / SPHERE_DIV;
      si = Math.sin(ai);
      ci = Math.cos(ai);

      vertices.push(si * sj * scale);  // X
      vertices.push(cj * scale);       // Y
      vertices.push(ci * sj * scale);  // Z
    }
  }
  translate(vertices, vertices, x, y, z);
  var result = [];
  for(var ii = 0; ii < vertices.length - (SPHERE_DIV * 3); ii += 3) {
    result.push(vertices[ii+0]);
    result.push(vertices[ii+1]);
    result.push(vertices[ii+2]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)+3]);

    result.push(vertices[ii+0+3]);
    result.push(vertices[ii+1+3]);
    result.push(vertices[ii+2+3]);

    result.push(vertices[ii+0]);
    result.push(vertices[ii+1]);
    result.push(vertices[ii+2]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)+3]);
  }
  return result;
}

function initLightNormals(vertices) {
  var result = [];
  var x1,y1,z1,x2,y2,z2,x3,y3,z3;
  for(var i = 0; i < vertices.length; i+=9){
    x1 = vertices[i+0]; y1 = vertices[i+1]; z1 = vertices[i+2];
    x2 = vertices[i+3]; y2 = vertices[i+4]; z2 = vertices[i+5];
    x3 = vertices[i+6]; y1 = vertices[i+7]; z1 = vertices[i+8];
    var norm = getNormal(
      [x1,y1,z1], [x2,y2,z2], [x3,y3,z3]
    )
    for(var ii = 0; ii < 3; ii++) {
      result.push(norm[0]);
      result.push(norm[1]);
      result.push(norm[2]);
    }
  }
  return result;
}

function getNormal(p1,p2,p3){
  var u = m4.subtractVectors(p1,p2);
  var w = m4.subtractVectors(p1,p3);
  var v = m4.cross(u,w);
  var v_dist = m4.distance(v, [0,0,0]);
  return [v[0]/v_dist, v[1]/v_dist, v[2]/v_dist];
}

function translate(src, dst, x, y, z) {
  for(var iii = 0; iii < src.length; iii+= 3) {
    dst[iii + 0] = src[iii + 0] + x;
    dst[iii + 1] = src[iii + 1] + y;
    dst[iii + 2] = src[iii + 2] + z;
  }
}
