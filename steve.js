/**
 * Object: Steve
 * Desc: Humanoid
 * Joints:
 * 1. body-head
 * 2. body-leftArm
 * 3. body-rightArm
 * 4. body-leftThigh
 * 5. body-rightThigh
 * 6. leftArm-leftForearm
 * 7. rightArm-rightForearm
 * 8. leftThigh-leftLeg
 * 9. rightThigh-rightLeg
 * 
 * Object Tree:
 *        Body
 *        /
 *      Head ----- leftArm  --------- rightArm  ---------- leftThigh  -----------  rightThigh
 *                  /                   /                     /                         /
 *               leftForearem         rightForearm        leftLeg                  rightLeg
 * 
 * 
 */
 
"use strict";

var canvas;
var gl;

var primitiveType;
var offset = 0;
var count = 132;

var angleCam = 0;
var angleFOV = 60;
var fRotationRadians = 0;

var matrix;

var translationMatrix;
var rotationMatrix;
var scaleMatrix;
var projectionMatrix;
var cameraMatrix;
var viewMatrix;
var viewProjectionMatrix;

var worldViewProjectionMatrix;
var worldInverseTransposeMatrix;
var worldInverseMatrix;
var worldMatrix;

var FOV_Radians; //field of view
var aspect; //projection aspect ratio
var zNear; //near view volume
var zFar; //far view volume

var cameraPosition = [50, 0, 100]; //eye/camera coordinates
var UpVector = [0, 1, 0]; //up vector
var fPosition = [0, 0, 0]; //at

var worldViewProjectionLocation;
var worldInverseTransposeLocation;
var colorLocation;
var lightWorldPositionLocation;
var worldLocation;

// list of all object to make dragon
var head; 
var object; // root of the object / body
var leftArm;
var leftForearm;
var rightArm;
var rightForearm;
var leftThigh;
var leftLeg;
var rightThigh;
var rightLeg;

// set the default rotation for each object
var objectXRot = 0, objectYRot = 0, objectZRot = 0,
headXRot = 0, headYRot = 0, headZRot = 0,
leftArmXRot = 0, leftArmYRot = 0, leftArmZRot = 0,
leftForearmXRot = 0, leftForearmYRot = 0, leftForearmZRot = 0,
rightArmXRot = 0, rightArmYRot = 0, rightArmZRot = 0,
rightForearmXRot = 0, rightForearmYRot = 0, rightForearmZRot = 0,
leftThighXRot = 0, leftThighYRot = 0, leftThighZRot = 0,
leftLegXRot = 0, leftLegYRot = 0, leftLegZRot = 0,
rightThighXRot = 0, rightThighYRot = 0, rightThighZRot = 0,
rightLegXRot = 0, rightLegYRot = 0, rightLegZRot = 0;

// animations variables
var animationToggle = false;
var angle = 0; // to store angle for animation

// what user currently select
var objectSelected;
var xRotationValue;
var yRotationValue;
var zRotationValue;

var program;

window.onload = function init() {
  canvas = document.getElementById("gl-canvas");

  gl = canvas.getContext("webgl2");
  if (!gl) alert("WebGL 2.0 isn't available");

  //  Configure WebGL

  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(1.0, 1.0, 1.0, 1.0);

  gl.enable(gl.CULL_FACE); //enable depth buffer
  gl.enable(gl.DEPTH_TEST);

  //initial default

  fRotationRadians = degToRad(0);
  FOV_Radians = degToRad(60);
  aspect = canvas.width / canvas.height;
  zNear = 1;
  zFar = 2000;

  initSteve();

  projectionMatrix = m4.perspective(FOV_Radians, aspect, zNear, zFar); //setup perspective viewing volume

  //  Load shaders and initialize attribute buffers
  program = initShaders(gl, "vertex-shader", "fragment-shader");
  gl.useProgram(program);

  // Load the data into the GPU

  worldViewProjectionLocation = gl.getUniformLocation(
    program,
    "u_worldViewProjection"
  );
  worldInverseTransposeLocation = gl.getUniformLocation(
    program,
    "u_worldInverseTranspose"
  );
  colorLocation = gl.getUniformLocation(program, "u_color");
  lightWorldPositionLocation = gl.getUniformLocation(
    program,
    "u_lightWorldPosition"
  );
  worldLocation = gl.getUniformLocation(program, "u_world");

  //update FOV
  fRotationRadians = degToRad(angleCam);

  xRotationValue = document.getElementById("xRotation");
  yRotationValue = document.getElementById("yRotation");
  zRotationValue = document.getElementById("zRotation");
  document.getElementById("object").value = objectSelected;
  document.getElementById("animate").onclick = function (event) {
    animationToggle = !animationToggle;
    render();
  }
  document.getElementById("object").onchange = function (event) {
    objectSelected = event.target.value;
    switch (objectSelected) {
      case "0":
        xRotationValue.value = radToDeg(headXRot);
        yRotationValue.value = radToDeg(headYRot);
        zRotationValue.value = radToDeg(headZRot);
        break;
      case "1":
        xRotationValue.value = radToDeg(objectXRot);
        yRotationValue.value = radToDeg(objectYRot);
        zRotationValue.value = radToDeg(objectZRot);
        break;
      case "2":
        xRotationValue.value = radToDeg(leftArmXRot);
        yRotationValue.value = radToDeg(leftArmYRot);
        zRotationValue.value = radToDeg(leftArmZRot);
        break;
      case "3":
        xRotationValue.value = radToDeg(leftForearmXRot);
        yRotationValue.value = radToDeg(leftForearmYRot);
        zRotationValue.value = radToDeg(leftForearmZRot);
        break;
      case "4":
        xRotationValue.value = radToDeg(rightArmXRot);
        yRotationValue.value = radToDeg(rightArmYRot);
        zRotationValue.value = radToDeg(rightArmZRot);
        break;
      case "5":
        xRotationValue.value = radToDeg(rightForearmXRot);
        yRotationValue.value = radToDeg(rightForearmYRot);
        zRotationValue.value = radToDeg(rightForearmZRot);
        break;
      case "6":
        xRotationValue.value = radToDeg(leftThighXRot);
        yRotationValue.value = radToDeg(leftThighYRot);
        zRotationValue.value = radToDeg(leftThighZRot);
        break;
      case "7":
        xRotationValue.value = radToDeg(leftLegXRot);
        yRotationValue.value = radToDeg(leftLegYRot);
        zRotationValue.value = radToDeg(leftLegZRot);
        break;
      case "8":
        xRotationValue.value = radToDeg(rightThighXRot);
        yRotationValue.value = radToDeg(rightThighYRot);
        zRotationValue.value = radToDeg(rightThighZRot);
        break;
      case "9":
        xRotationValue.value = radToDeg(rightLegXRot);
        yRotationValue.value = radToDeg(rightLegYRot);
        zRotationValue.value = radToDeg(rightLegZRot);
        break;
    }
  }
  document.getElementById("xRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        headXRot = degToRad(event.target.value);
        break;
      case "1":
        objectXRot = degToRad(event.target.value);
        break;
      case "2":
        leftArmXRot = degToRad(event.target.value);
        break;
      case "3":
        leftForearmXRot = degToRad(event.target.value);
        break;
      case "4":
        rightArmXRot = degToRad(event.target.value);
        break;
      case "5":
        rightForearmXRot = degToRad(event.target.value);
        break;
      case "6":
        leftThighXRot = degToRad(event.target.value);
        break;
      case "7":
        leftLegXRot = degToRad(event.target.value);
        break;
      case "8":
        rightThighXRot = degToRad(event.target.value);
        break;
      case "9":
        rightLegXRot = degToRad(event.target.value);
        break;
    }
    render();
  }
  document.getElementById("yRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        headYRot = degToRad(event.target.value);
        break;
      case "1":
        objectYRot = degToRad(event.target.value);
        break;
      case "2":
        leftArmYRot = degToRad(event.target.value);
        break;
      case "3":
        leftForearmYRot = degToRad(event.target.value);
        break;
      case "4":
        rightArmYRot = degToRad(event.target.value);
        break;
      case "5":
        rightForearmYRot = degToRad(event.target.value);
        break;
      case "6":
        leftThighYRot = degToRad(event.target.value);
        break;
      case "7":
        leftLegYRot = degToRad(event.target.value);
        break;
      case "8":
        rightThighYRot = degToRad(event.target.value);
        break;
      case "9":
        rightLegYRot = degToRad(event.target.value);
        break;
    }
    render();
  };
  document.getElementById("zRotation").onchange = function (event) {
    switch (objectSelected) {
      case "0":
        headZRot = degToRad(event.target.value);
        break;
      case "1":
        objectZRot = degToRad(event.target.value);
        break;
      case "2":
        leftArmZRot = degToRad(event.target.value);
        break;
      case "3":
        leftForearmZRot = degToRad(event.target.value);
        break;
      case "4":
        rightArmZRot = degToRad(event.target.value);
        break;
      case "5":
        rightForearmZRot = degToRad(event.target.value);
        break;
      case "6":
        leftThighZRot = degToRad(event.target.value);
        break;
      case "7":
        leftLegZRot = degToRad(event.target.value);
        break;
      case "8":
        rightThighZRot = degToRad(event.target.value);
        break;
      case "9":
        rightLegZRot = degToRad(event.target.value);
        break;
    }
    render();
  };
  primitiveType = gl.TRIANGLES;
  render(); //default render
};

function render() {
  // Compute the camera's matrix using look at.
  cameraMatrix = m4.lookAt(cameraPosition, fPosition, UpVector);

  // Make a view matrix from the camera matrix
  viewMatrix = m4.inverse(cameraMatrix);

  // Compute a view projection matrix
  viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

  worldMatrix = m4.identity();

  // Multiply the matrices.
  worldViewProjectionMatrix = m4.multiply(viewProjectionMatrix, worldMatrix);
  worldInverseMatrix = m4.inverse(worldMatrix);
  worldInverseTransposeMatrix = m4.transpose(worldInverseMatrix);

  // Set the matrices
  gl.uniformMatrix4fv(
    worldViewProjectionLocation,
    false,
    worldViewProjectionMatrix
  );
  gl.uniformMatrix4fv(
    worldInverseTransposeLocation,
    false,
    worldInverseTransposeMatrix
  );
  gl.uniformMatrix4fv(worldLocation, false, worldMatrix);

  // Set the color to use
  gl.uniform4fv(colorLocation, [0.2, 1, 0.2, 1]); // green

  // set the light direction.
  gl.uniform3fv(lightWorldPositionLocation, [30, 0, 30]);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // since we will compute each object independently,
  // we manipulate vertices before sending to GPU
  verticesManipulation();
  angle += 1;

  // init light vertices and normals
  var lightVertices = initLight(30, 0, 30, 5);
  var lightNormals = initLightNormals(lightVertices);
  
  // send manipulated vertices to GPU
  var n = initBuffers(new Float32Array(object.getGeometry().concat(lightVertices)));
  initNormals(new Float32Array(object.getNormals().concat(lightNormals)));

  // draw
  gl.drawArrays(primitiveType, offset, n);

  // reset object to be transform for later on
  resetObject();

  // re-render for animation
  if(animationToggle) requestAnimationFrame(render);
}

function verticesManipulation() {
  if(animationToggle) {
    var angleInRadians = 360 - (angle * Math.PI) / 180;
    var sin = Math.sin(angleInRadians);
    head.rotate(0,0,sin * Math.PI/5);
    leftForearm.rotate(0,0, -(sin * Math.PI/10));
    leftArm.rotate(-Math.PI, 0, -(sin * Math.PI/5));
    rightForearm.rotate(0,0, -(sin * Math.PI/10));
    rightArm.rotate(-Math.PI, 0, -(sin * Math.PI/5));
    leftLeg.rotate(0,0,Math.PI/20 - (sin * Math.PI/20));
    leftThigh.rotate(0,0,Math.PI/10 - (sin * Math.PI/10));
    rightLeg.rotate(0,0,-(sin * Math.PI/20) - Math.PI/20);
    rightThigh.rotate(0,0,-(sin * Math.PI/10) - Math.PI/10);
  } else {
    rightLeg.rotate(rightLegXRot, rightLegYRot, rightLegZRot);
    rightThigh.rotate(rightThighXRot, rightThighYRot, rightThighZRot);
    leftLeg.rotate(leftLegXRot, leftLegYRot, leftLegZRot);
    leftThigh.rotate(leftThighXRot, leftThighYRot, leftThighZRot);
    rightForearm.rotate(rightForearmXRot, rightForearmYRot, rightForearmZRot);
    rightArm.rotate(rightArmXRot, rightArmYRot, rightArmZRot);
    leftForearm.rotate(leftForearmXRot, leftForearmYRot, leftForearmZRot);
    leftArm.rotate(leftArmXRot, leftArmYRot, leftArmZRot);
    head.rotate(headXRot, headYRot, headZRot);
    object.rotate(objectXRot, objectYRot, objectZRot);
  }
}

function resetObject() {
  // reset all object to default position for later
  // transformations
  object.reset();
  head.reset();
  leftArm.reset();
  leftForearm.reset();
  rightArm.reset();
  rightForearm.reset();
  leftThigh.reset();
  leftLeg.reset();
  rightThigh.reset();
  rightLeg.reset();
}

/* Create buffers for multiple objects */
function initBuffers(vertices) {
  var n = vertices.length / 3;

  var positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);

  var positionLocation = gl.getAttribLocation(program, "a_position");
  gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(positionLocation);

  return n;
}

/* Create normal buffers for multiple objects */
function initNormals(normals) {
  var normalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);

  // Associate out shader variables with our data buffer

  var normalLocation = gl.getAttribLocation(program, "a_normal");
  gl.vertexAttribPointer(normalLocation, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(normalLocation);
}

function radToDeg(r) {
  return (r * 180) / Math.PI;
}

function degToRad(d) {
  return (d * Math.PI) / 180;
}

function deepCopy(source, target) {
  for(var i = 0; i < source.length; i++) {
    target[i] = source[i] + 1 - 1; // to force deep copy instead of shallow
  }
}

function initSteve() {
  // basic normals
  var cubeNormals = [
    // top
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    0, -1, 0,
    // left
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    1, 0, 0,
    // bottom
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    0, 1, 0,
    // right
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    -1, 0, 0,
    // front
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    0, 0, -1,
    // back
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
    0, 0, 1,
  ];

  // body
  var objectNomarls = []
  deepCopy(cubeNormals, objectNomarls);
  object = new ObjectNode([
    // top
    -5, -10, -2.5,
    5, -10, -2.5,
    5, -10, 2.5,
    -5, -10, -2.5,
    5, -10, 2.5,
    -5, -10, 2.5,
    // left
    5, -10, -2.5,
    5, 10, -2.5,
    5, -10, 2.5,
    5, 10, -2.5,
    5, 10, 2.5,
    5, -10, 2.5,
    // bottom
    5, 10, -2.5,
    -5, 10, -2.5,
    5, 10, 2.5,
    -5, 10, -2.5,
    -5, 10, 2.5,
    5, 10, 2.5,
    // right
    -5, -10, -2.5,
    -5, -10, 2.5,
    -5, 10, -2.5,
    -5, -10, 2.5,
    -5, 10, 2.5,
    -5, 10, -2.5,
    // front
    -5, -10, -2.5,
    -5, 10, -2.5,
    5, 10, -2.5,
    -5, -10, -2.5,
    5, 10, -2.5,
    5, -10, -2.5,
    // back
    -5, -10, 2.5,
    5, -10, 2.5,
    5, 10, 2.5,
    -5, -10, 2.5,
    5, 10, 2.5,
    -5, 10, 2.5,
  ],objectNomarls,[0,0,0]);

  // head
  var headNormals = [];
  deepCopy(cubeNormals, headNormals);
  head = new ObjectNode([
    // top
    -5, -20, -5,
    5, -20, -5,
    5, -20, 5,
    -5, -20, -5,
    5, -20, 5,
    -5, -20, 5,
    // left
    5, -20, -5,
    5, -10, -5,
    5, -20, 5,
    5, -10, -5,
    5, -10, 5,
    5, -20, 5,
    // bottom
    5, -10, -5,
    -5, -10, -5,
    5, -10, 5,
    -5, -10, -5,
    -5, -10, 5,
    5, -10, 5,
    // right
    -5, -20, -5,
    -5, -20, 5,
    -5, -10, -5,
    -5, -20, 5,
    -5, -10, 5,
    -5, -10, -5,
    // front
    -5, -20, -5,
    -5, -10, -5,
    5, -10, -5,
    -5, -20, -5,
    5, -10, -5,
    5, -20, -5,
    // back
    -5, -20, 5,
    5, -20, 5,
    5, -10, 5,
    -5, -20, 5,
    5, -10, 5,
    -5, -10, 5,
  ],headNormals,[0,-10,0]);

  var armLegsPart = [
    // top
    0, 0, 0,
    5, 0, 0,
    5, 0, 5,
    0, 0, 0,
    5, 0, 5,
    0, 0, 5,
    // left
    5, 0, 0,
    5, 10, 0,
    5, 10, 5,
    5, 0, 0,
    5, 10, 5,
    5, 0, 5,
    // bottom
    0, 10, 0,
    0, 10, 5,
    5, 10, 0,
    0, 10, 5,
    5, 10, 5,
    5, 10, 0,
    // right
    0, 0, 0,
    0, 0, 5,
    0, 10, 5,
    0, 0, 0,
    0, 10, 5,
    0, 10, 0,
    // front
    0, 0, 0,
    0, 10, 0,
    5, 10, 0,
    0, 0, 0,
    5, 10, 0,
    5, 0, 0,
    // back
    0, 0, 5,
    5, 0, 5,
    5, 10, 5,
    0, 0, 5,
    5, 10, 5,
    0, 10, 5,
  ];

  // left arm
  var leftArmGeo = [];
  var leftArmNormals = [];
  translate(armLegsPart, leftArmGeo, 5, -10, -2.5);
  deepCopy(cubeNormals, leftArmNormals);
  leftArm = new ObjectNode(leftArmGeo, leftArmNormals, [5, -10, 0]);

  // left forearm
  var leftForearmGeo = [];
  var leftForearmNormals = [];
  translate(armLegsPart, leftForearmGeo, 5, 0, -2.5);
  deepCopy(cubeNormals, leftForearmNormals);
  leftForearm = new ObjectNode(leftForearmGeo, leftForearmNormals, [7.5, 0, 0]);

  // right arm
  var rightArmGeo = [];
  var rightArmNormals = [];
  translate(armLegsPart, rightArmGeo, -10, -10, -2.5);
  deepCopy(cubeNormals, rightArmNormals);
  rightArm = new ObjectNode(rightArmGeo, rightArmNormals, [-5, -10, 0]);

  // right forearm
  var rightForearmGeo = [];
  var rightForearmNormals = [];
  translate(armLegsPart, rightForearmGeo, -10, 0, -2.5);
  deepCopy(cubeNormals, rightForearmNormals);
  rightForearm = new ObjectNode(rightForearmGeo, rightForearmNormals, [-7.5, 0, 0]);

  // left thigh
  var leftThighGeo = [];
  var leftThighNormals = [];
  translate(armLegsPart, leftThighGeo, 0, 10, -2.5);
  deepCopy(cubeNormals, leftThighNormals);
  leftThigh = new ObjectNode(leftThighGeo, leftThighNormals, [2.5, 10, 0]);

  // left leg
  var leftLegGeo = [];
  var leftLegNormals = [];
  translate(armLegsPart, leftLegGeo, 0, 20, -2.5);
  deepCopy(cubeNormals, leftLegNormals);
  leftLeg = new ObjectNode(leftLegGeo, leftLegNormals, [2.5, 20, 0]);

  // right thigh
  var rightThighGeo = [];
  var rightThighNormals = [];
  translate(armLegsPart, rightThighGeo, -5, 10, -2.5);
  deepCopy(cubeNormals, rightThighNormals);
  rightThigh = new ObjectNode(rightThighGeo, rightThighNormals, [-2.5, 10, 0]);

  // right leg
  var rightLegGeo = [];
  var rightLegNormals = [];
  translate(armLegsPart, rightLegGeo, -5, 20, -2.5);
  deepCopy(cubeNormals, rightLegNormals);
  rightLeg = new ObjectNode(rightLegGeo, rightLegNormals, [-2.5, 20, 0]);

  object.setChild(head);
  head.setSibling(leftArm);
  leftArm.setChild(leftForearm);
  leftArm.setSibling(rightArm);
  rightArm.setChild(rightForearm);
  rightArm.setSibling(leftThigh);
  leftThigh.setChild(leftLeg);
  leftThigh.setSibling(rightThigh);
  rightThigh.setChild(rightLeg);
}

function initLight(x, y, z, scale) {
  var SPHERE_DIV = 12;
  var i, ai, si, ci;
  var j, aj, sj, cj;
  var p1, p2;

  // Vertices
  var vertices = [];
  for (j = 0; j <= SPHERE_DIV; j++) {
    aj = j * Math.PI / SPHERE_DIV;
    sj = Math.sin(aj);
    cj = Math.cos(aj);
    for (i = 0; i < SPHERE_DIV; i++) {
      ai = i * 2 * Math.PI / SPHERE_DIV;
      si = Math.sin(ai);
      ci = Math.cos(ai);

      vertices.push(si * sj * scale);  // X
      vertices.push(cj * scale);       // Y
      vertices.push(ci * sj * scale);  // Z
    }
  }
  translate(vertices, vertices, x, y, z);
  var result = [];
  for(var ii = 0; ii < vertices.length - (SPHERE_DIV * 3); ii += 3) {
    result.push(vertices[ii+0]);
    result.push(vertices[ii+1]);
    result.push(vertices[ii+2]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)+3]);

    result.push(vertices[ii+0+3]);
    result.push(vertices[ii+1+3]);
    result.push(vertices[ii+2+3]);

    result.push(vertices[ii+0]);
    result.push(vertices[ii+1]);
    result.push(vertices[ii+2]);

    result.push(vertices[ii+0+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+1+(SPHERE_DIV*3)+3]);
    result.push(vertices[ii+2+(SPHERE_DIV*3)+3]);
  }
  return result;
}

function initLightNormals(vertices) {
  var result = [];
  var x1,y1,z1,x2,y2,z2,x3,y3,z3;
  for(var i = 0; i < vertices.length; i+=9){
    x1 = vertices[i+0]; y1 = vertices[i+1]; z1 = vertices[i+2];
    x2 = vertices[i+3]; y2 = vertices[i+4]; z2 = vertices[i+5];
    x3 = vertices[i+6]; y1 = vertices[i+7]; z1 = vertices[i+8];
    var norm = getNormal(
      [x1,y1,z1], [x2,y2,z2], [x3,y3,z3]
    )
    for(var ii = 0; ii < 3; ii++) {
      result.push(norm[0]);
      result.push(norm[1]);
      result.push(norm[2]);
    }
  }
  return result;
}

function getNormal(p1,p2,p3){
  var u = m4.subtractVectors(p1,p2);
  var w = m4.subtractVectors(p1,p3);
  var v = m4.cross(u,w);
  var v_dist = m4.distance(v, [0,0,0]);
  return [v[0]/v_dist, v[1]/v_dist, v[2]/v_dist];
}

function translate(src, dst, x, y, z) {
  for(var iii = 0; iii < src.length; iii+= 3) {
    dst[iii + 0] = src[iii + 0] + x;
    dst[iii + 1] = src[iii + 1] + y;
    dst[iii + 2] = src[iii + 2] + z;
  }
}


